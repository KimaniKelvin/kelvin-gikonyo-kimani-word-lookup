package com.example.word_lookup_mini_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<String> swahiliWords, englishWords;
    private Button btnExit;
    private Button btnTranslate;
    private EditText editWord;
    private TextView prompt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        swahiliWords = new ArrayList<>();
        englishWords = new ArrayList<>();
        btnExit = findViewById(R.id.btnExit);
        btnTranslate = findViewById(R.id.btnTranslate);
        editWord = findViewById(R.id.editWord);
        prompt = findViewById(R.id.prompt);

        loadCsvFile();

        btnTranslate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                translateText();
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void loadCsvFile(){
        InputStream inputStream = getResources().openRawResource(R.raw.words);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try {
            String line;
            while ((line = reader.readLine()) != null){
                try {
                    String[] words = line.split(",");
                    if (words.length == 2){
                        swahiliWords.add(words[0]);
                        englishWords.add(words[1]);

                    }
                }catch (Exception e){
                    Log.e("Error", e.toString());
                }
            }
        }
        catch (IOException io){
            Log.e("Error", io.toString());
        }
    }

    private void translateText(){
        String word = editWord.getText().toString().trim();
        String transMSg = "";
        ArrayList<String> words = new ArrayList<>();

        if (englishWords.contains(word)){
            transMSg = "Tafsiri ya neno hilo ni";

            for (int i=0; i<englishWords.size(); i++){
                if (englishWords.get(i).equals(word)){
                    words.add(swahiliWords.get(i));
                }
            }
        }

        else if (swahiliWords.contains(word)){
            transMSg = "The translations are: ";
            for(int i=0; i<swahiliWords.size(); i++){
                if (swahiliWords.get(i).equals(word)){
                    words.add(englishWords.get(i));
                }
            }
        }

        else {
            Toast.makeText(getApplicationContext(),"No such word found in the dictionary",Toast.LENGTH_SHORT).show();

        }

        prompt.setText(transMSg);

        Adapter adapter = new Adapter(words,this);
        RecyclerView view = findViewById(R.id.recyclerView);
        view.setAdapter(adapter);
        view.setLayoutManager(new LinearLayoutManager(this));

    }
}
